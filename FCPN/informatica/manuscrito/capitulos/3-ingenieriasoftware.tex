%!TEX TS-program = lualatex
%!TEX encoding = UTF-8 Unicode
% 3-ingenieriasoftware.tex
% 
% Proyecto teXisUMSA
%
% Éste archivo ha sido descargado desde:
% https://gitlab.com/amartin1911/teXisUMSA
%
% Autor:
% Álvaro M. Callejas <amcallejasherrera@gmail.com>
%
% Licencia de las plantillas(archivos): 
% This file is part of teXisUMSA.
% 
%     teXisUMSA is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
% 
%     teXisUMSA is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
% 
%     You should have received a copy of the GNU General Public License
%     along with teXisUMSA.  If not, see <https://www.gnu.org/licenses/>.
%
% Licencia del contenido:
% CC BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/)
%


\documentclass[../tesis.tex]{subfiles}
\begin{document}
	Es necesario recordar que la ingeniería de software no se trata únicamente 
	de una metodología sino, tal como señala 
	\textcite{PressmanSoftwareEngineeringPractitioner2010}, también de una 
	\textquote{tecnología de varias capas}. En este sentido, el presente 
	capítulo agrupa tanto la teoría general de ingeniería de software, así como 
	la documentación de las tecnologías utilizadas durante el desarrollo de 
	todo el trabajo.
	
	Dichas tecnologías son: OpenUP, el proceso para todo el ciclo de trabajo; 
	\acrshort{uml}, el modelo de abstracción para el análisis de 
	requerimientos; Python, el principal lenguaje para la programación; Git, 
	Pyenv y Pipenv, herramientas del entorno de desarrollo y finalmente el 
	motor 
	gráfico para la implementación de la interfaz gráfica, \acrshort{gtk}.
		
	\section{Importancia de la Ingeniería de Software}
	El software se ha integrado prácticamente en todos los 
	aspectos de nuestras vidas, y como consecuencia el número de personas que 
	tienen interés en las características y funciones proporcionadas por el 
	mismo ha crecido de forma espectacular. 
	
	Cada falla en el software indica un error en el diseño o en el proceso a 
	través del cual el diseño fue implementado. De acuerdo a 
	\textcite{PressmanSoftwareEngineeringPractitioner2010}, la clave para que 
	el desarrollador pueda construir software capaz de enfrentar los retos 
	del siglo XXI reside en el reconocimiento de tres realidades:

	\begin{enumerate}[label=(\alph*)]
		\item Se debe entender el problema, en consenso con todas las partes 
		involucradas, antes de brindar una solución.
		\item El software se vuelve más sofisticado, con interacciones 
		complejas entre elementos. Por este motivo, el diseño es una actividad 
		fundamental de la ingeniería de software.
		\item Ya que nuestra sociedad depende cada vez más del software para 
		tomar decisiones, el mismo debe tener alta calidad. Asimismo, debe 
		tener facilidad para recibir mantenimiento a largo plazo.
	\end{enumerate}
	
	Estas realidades simples llevan a una conclusión: debe hacerse ingeniería 
	de software en todas sus formas y a través de todos sus dominios de 
	aplicación.

	\subsection{Capas}
	
	Como se mencionó anteriormente, la ingeniería de software es una tecnología 
	con varias capas, las cuales se observan en la \autoref{fig:capasingsoft}. 
	Los fundamentos son el compromiso 
	organizacional con la calidad, y el proceso. Este último es el que define 
	la estructura, el marco de trabajo, estableciendo el contexto para la 
	generación de productos del trabajo, como ser modelos, documentos, 
	reportes, entre 
	otros. Los métodos incluyen un conjunto amplio de tareas, como ser 
	comunicación, análisis de requerimientos, modelación, y más. Finalmente, 
	las herramientas proporcionan un apoyo automatizado, o semiautomatizado 
	para el proceso y los métodos.

	\begin{figure}[htb]
		\centering
		\includegraphics[height=4cm]{capasingsoft}
		\caption{Capas de la ingeniería de software}
		\small{Fuente: \textcite{PressmanSoftwareEngineeringPractitioner2010}}
		\label{fig:capasingsoft}
	\end{figure}
	
	\section{Proceso OpenUP}	
	%	Según el IEEE la ingeniería de software se define así:
	%	\todo{Pressman p42. Encontrar definición en español}
	%	Aun así, un acercamiento \textquote{sistemático, disciplinado y 
	%	cuantificable} aplicado por un equipo de software puede ser una carga 
	%para 
	%	otro. Se necesita disciplina, pero también adaptabilidad y agilidad.
	
	Unified Process (UP), Proceso Unificado de Desarrollo de Software, o
	simplemente Proceso Unificado, es un marco de desarrollo de software
	genérico y adaptable, caracterizado por ser dirigido mediante casos 
	de uso,
	estar centrado en la arquitectura, y por ser iterativo e
	incremental~\parencite{Jacobsonunifiedsoftwaredevelopment1999}. De 
	entre
	los procesos basados en UP --- por ejemplo RUP, ESSUP, OpenUP, EUP 
	y AUP
	--- los dos más notables, según
	\textcite{KrebsAgileportfoliomanagement2008}, son RUP y OpenUP.
	
	En esencia el \acrfull{openup}, es un proceso ágil, 
	ligero,
	completo y extensible que engloba las mejores y bien conocidas 
	prácticas
	del desarrollo de software. Comenzó a finales del año 2005 gracias 
	a la
	donación de IBM~\parencite{IBMNewsroom} de una parte del proceso de 
	RUP a
	la comunidad Open Source, especificamente a la Eclipse Foundation.
	
	Primeramente conocido como Basic Unified Process (BUP), fue 
	renombrado a
	comienzos de 2006 a OpenUP/Basic y publicado bajo la licencia
	EPL\footnote{Eclipse Public License (EPL) es una licencia de 
		software Open
		Source aprobada por la Open Source Initiative (OSI) y listada 
		como 
		licencia
		de software libre --- incompatible con la licencia GPL --- por 
		la Free
		Software Foundation (FSF).}. Ahora es conocido simplemente como 
	\acrshort{openup}, y
	está definido como un proceso ejemplar de la EPF, complementándose 
	con la
	herramienta de software EPF Composer, una plataforma que permite la 
	creación, adaptación y despliegue de procesos.
	
%	Es la herramienta 
%	ejemplar de	EPF. EPF Composer es considerado el hermano
%	menor~\parencite{RationalMethodComposer} de Rational Method
%	Composer\footnote{Software (plataforma) de gestión de procesos. Su
%		licencia es propietaria y pertenece a la familia de productos
%		Rational de IBM}, existiendo compatibilidad entre ambas 
%	herramientas
	
	\subsection{Estructura}	
	OpenUP estructura el ciclo de vida del proyecto en iteraciones de 
	cuatro
	fases: Inicio, Elaboración, Construcción y Transición. Una visión del ciclo 
	de vida, y las capas de OpenUP se pueden observar en la 
	\autoref{fig:openupcapas}.\todo{Explicar figura}
	
	\begin{figure}[htb]
		\centering
		\includegraphics[height=7cm]{openupcapas}
		\caption{Capas y ciclo del proceso OpenUP}
		\small{Fuente:~\textcite{anoryatOpenUP2008}}
		\label{fig:openupcapas}	
	\end{figure}
	
	El ciclo de vida de un proyecto según \acrshort{openup}, permite que 
	los integrantes del equipo de
	desarrollo aporten con micro-incrementos, que pueden ser el resultado del 
	trabajo de unas pocas horas o unos
	pocos días. El progreso se puede visualizar diariamente, ya que la 
	aplicación va evolucionando en función de
	estos micro-incrementos.
	
	Todo proyecto en OpenUP consta de cuatro fases: inicio, elaboración, 
	construcción y transición. Cada una
	de estas fases se divide a su vez en iteraciones. A continuación se 
	describen estas fases y su relación:
	\begin{itemize}
		\item Fase de inicio. En esta fase, las necesidades de cada 
		participante del proyecto son tomadas en
		cuenta y plasmadas en objetivos del proyecto. Se definen para el 
		proyecto: el ámbito, los límites, el
		criterio de aceptación, los casos de uso críticos, una estimación 
		inicial del coste y un boceto de la
		planificación.

		\item Fase de elaboración. En esta fase se realizan tareas de análisis 
		del dominio y definición de la arquitectura del sistema. Se debe 
		elaborar un plan de proyecto, 
		estableciendo unos requisitos y una
		arquitectura estables. Por otro lado, el proceso de desarrollo, las 
		herramientas, la infraestructura a
		utilizar y el entorno de desarrollo también se especifican en detalle 
		en esta fase. Al final de la fase se
		debe tener una definición clara y precisa de los casos de uso, los 
		actores, la arquitectura del sistema
		y un prototipo ejecutable de la misma.

		\item Fase de construcción. Todos los componentes y funcionalidades del 
		sistema que falten por implementar son realizados, probados e 
		integrados en esta fase. Los resultados obtenidos en forma de
		incrementos ejecutables deben ser desarrollados de la forma más rápida 
		posible sin dejar de lado la
		calidad de lo desarrollado.

		\item Fase de transición. Esta fase corresponde a la introducción del 
		producto en la comunidad de usuarios, cuando el producto está lo 
		suficientemente maduro. La fase de la transición consta de las sub	
		fases de pruebas de versiones beta, pilotaje y capacitación de los 
		usuarios finales y de los encargados del mantenimiento del sistema. En 
		función de la respuesta obtenida por los usuarios puede ser
		necesario realizar cambios en las entregas finales o implementar alguna 
		funcionalidad más.
	\end{itemize}
	
	\section{Lenguaje Unificado de Modelado UML}	
	En esencia, los modelos permiten describir sistemas de manera eficiente 
	y elegante. Cuando se crean sistemas 
	de software es extremadamente importante seleccionar medios adecuados de 
	abstracción: por un lado para la implementación, pero por otro también para 
	el uso posterior de los mismos.	
	
	La correcta elección de medios de abstracción hace 
	la programación mucho más fácil. Las partes individuales tienen interfaces 
	sencillas y pequeñas, y nuevas funcionalidades pueden ser introducidas sin 
	la necesidad de una extensiva refactorización.
	
	Los modelos deben ser creados con gran cuidado y debida consideración. De 
	acuerdo a \textcite{Selicpragmaticsmodeldrivendevelopment2003}, cinco 
	características determinan la calidad de un modelo:
	
	\begin{itemize}
		\item Abstracción: un modelo siempre es una representación reducida del 
		sistema 
		que representa, así para el usuario es más fácil entender la esencia 
		del 
		todo sin entrar en detalles.		
		\item Comprensibilidad: que es resultado directo de la expresividad del 
		lenguaje 
		de modelado. La expresividad puede ser definida como la habilidad de 
		presentar 
		contenido complejo con tan pocos conceptos como sea posible.		
		\item Exactitud: el modelo debe resaltar las propiedades relevantes del 
		sistema real.		
		\item Predictividad: un modelo debe permitir la predicción de 
		propiedades interesantes, pero no obvias, del sistema modelado.		
		\item Rentabilidad: a largo plazo, debe ser más barato crear el modelo 
		que crear el sistema modelado
		
	\end{itemize}
	
	Hoy en día el lenguaje de modelado visual orientado a objetos más extendido 
	y utilizado es es \acrfull{uml}, un lenguaje de modelado de propósito 
	general que permite 
	elaborar abstracciones de sistemas de software. Provee 
	conceptos de lenguaje y modelado, además de una intuitiva notación gráfica, 
	que permiten a un sistema de software ser especificado, diseñado, 
	visualizado y documentado.
	
	\acrshort{uml} puede ser utilizado 
	consistentemente a través de todo el proceso de desarrollo de software y no 
	esta atado a una herramienta de desarrollo, 
	lenguaje de programación, o plataforma en específico. Por otro lado, es 
	importante mencionar que UML es sólo una herramienta de 
	documentación y no un proceso de desarrollo de software.
	
	\subsection{Diagramas}
	En \acrshort{uml}, un modelo es representado gráficamente en forma de 
	diagramas. Un diagrama proporciona una visión de esa porción de la realidad 
	descrita por el modelo. En su actual versión 2.5.1, UML cuenta 
	con 14 diagramas 
	que describen tanto la estructura como el comportamiento de un sistema. La 
	\autoref{fig:umldiagramas} ofrece una taxonomía de 
	los mismos.

	\begin{figure}[htb]
		\centering
		\scriptsize
		\def\svgwidth{0.7\columnwidth}
		\input{figuras/umldiagramas.pdf_tex}
		\caption{Diagramas de UML 2.5}
		\small{Fuente: Adaptado de \textcite{SeidlUMLclassroomintroduction2015}}
%		\captionsource{Diagramas UML}{Adaptado de \textcite{SeidlUMLclassroomintroduction2015}}
		\label{fig:umldiagramas}	
	\end{figure}
	
	%	p15 añadir propiedades de los diagramas
	De todos los diagramas UML, son cinco los más importantes y 
	extendidos. Están listados a continuación en el orden en el cual serían 
	generalmente utilizados en un proyecto de desarrollo de software.
	\begin{enumerate}
		\item Diagrama de Casos de Uso
		\item Diagrama de Clases (incluyendo el Diagrama de Objetos)
		\item Diagrama de Estados
		\item Diagrama de Secuencias
		\item Diagrama de Actividades
	\end{enumerate}
	
	El diagrama de casos de uso especifica la funcionalidad básica del sistema. 
	Luego, el diagrama de clases define cuales objetos o clases están 
	involucradas en la realización de esa funcionalidad. Después, el diagrama 
	de estados define el comportamiento interno de los objetos, mientras que el 
	diagrama de secuencias especifica el comportamiento entre objetos. 
	Finalmente, el diagrama de actividades permite definir esos procesos que 
	\textquote{implementan} los casos de uso del diagrama de casos de uso.
	
%	\section{Software libre}
%	Una tendencia en crecimiento que resulta en la distribución del código 
%	fuente del software para que otras personas puedan contribuir a su 
%	desarrollo. El reto para los ingenieros de software\todo{parafrasear: 
%	enfoque mas general, no solo ing.software}, es desarrollar código fuente 
%	que sea autodescriptivo, pero lo más importante, desarrollar técnicas que 
%	permitan a los clientes y desarrolladores saber qué cambios se han 
%	realizado y cómo esos cambios se manifiestan dentro del software
%	% pressman p38
	\section{Lenguaje Python}
	\label{sec:lengpython}
	Python es un lenguaje de programación de propósito general, de alto nivel, 
	de tipado fuerte y dinámico. El tipado fuerte significa que el tipo de un 
	valor no cambia repentinamente. Una cadena que contiene solo dígitos no se 
	convierte \textquote{mágicamente} en un número tal cual podría suceder en 
	otro lenguaje,  como ser Perl. Cada cambio de tipo requiere una conversión 
	explícita. El tipado 
	dinámico significa que objetos en tiempo de ejecución, es decir, valores 
	tienen un 
	tipo, a diferencia del tipado estático donde las variables tienen un tipo. 
	Es decir, en Python no es necesario declarar el tipo de las variables, 
	estas empiezan 
	a \textquote{existir} cuando se les asigna un valor.
	
	Según~\textcite{GuttagIntroductioncomputationprogramming2013}, debido a su 
	comprobación débil de semántica estática ---Python es de tipado 
	dinámico---, Python no es óptimo para programas que tienen restricciones de 
	alta 
	fiabilidad, o que son construidos y mantenidos por muchas personas en un 
	largo periodo de tiempo.
	
	A pesar de la observación previa, Python tiene bastantes ventajas sobre 
	muchos otros 
	lenguajes. Es un lenguaje relativamente simple y fácil de aprender, está 
	enfocado en la usabilidad, permite a los programadores expresar conceptos 
	en menos líneas de código de lo que sería posible en muchos otros 
	lenguajes, como C, soporta múltiples paradigmas de programación y tiene 
	construcciones destinadas a crear programas 
	claros en una variedad de dominios\footnote{Una muestra del uso de Python 
		en diferentes aplicaciones puede encontrarse en 
		\url{https://www.python.org/about/apps/}.}
	
	A diferencia de otros lenguajes, Python usa una sintaxis basada en 
	indentación, es decir, sangría. No hay llaves que marquen dónde empieza o 
	termina 
	un bloque código ya que los mismos se definen por su sangrado y el uso de 
	dos puntos \textquote{:}. El sangrado no es una cuestión de estilo sino un 
	requisito del lenguaje. Además, para separar 
	sentencias utiliza retornos de carro, a diferencia de, por ejemplo, Java, 
	que utiliza punto y coma \textquote{;} y podría albergar más de una 
	sentencia en una sola línea de código. Esto hace más sencilla la lectura y 
	comprensión del 
	código en Python escrito por otras personas. Inclusive existe la 
	\textquote{Guía de 
	Estilo para Código Python}, conocida como PEP8\footnote{Una PEP, del inglés 
		\textit{Python Enhancement Proposal} es una Propuesta de Mejora para 
		Python. Describe los cambios en Python, o los estándares a su 
		alrededor. 
		Las más notables son la 8, 20 y 257.}, para hacer que el código en 
		Python 
	sea más legible y consistente en todas partes.
	
	\subsection{Historia y licencia}
	Python fue creado a comienzos de los años 90 por Guido van Rossum en el 
	\textit{Stichting Mathematisch Centrum} (CWI) de Holanda como el sucesor de 
	un 
	lenguaje llamado ABC. El principal autor de Python continúa siendo van 
	Rossum,	aunque también incluye muchas contribuciones de otros.
	
	En 1995, van Rossum continuó su trabajo en Python en el \textit{Corporation 
		for National Research Initiatives} (CNRI) en Reston, Virginia, EEUU, 
		donde 
	lanzó varias versiones del software.
	
	En 2001, se formó la \acrfull{psf}, una 
	organización sin fines de lucro específicamente creada para ser titular de 
	la propiedad intelectual relacionada con Python.
	
	Python está desarrollado bajo una licencia \textit{Open Source}, propiedad 
	de la PSF, aprobada 
	por la \acrfull{osi}, haciéndolo libre para su uso y distribución, incluso 
	con fines comerciales \parencite{PSFPython2018}. Históricamente la mayoría, 
	pero no todos, los 
	lanzamientos de Python también han sido compatibles con la \acrfull{gpl}.
	
	\subsection{Elementos básicos}
	En \textit{Introduction to computation and programming using 
		Python}~\parencite{GuttagIntroductioncomputationprogramming2013}, 
		presenta 
	los conceptos y elementos básicos de la programación en Python, los cuales 
	se exponen brevemente a continuación. Un programa en Python, a veces 
	llamado \textit{script}, es una secuencia de 
	definiciones y comandos. Estas definiciones son evaluadas y los comandos 
	son ejecutados por el intérprete de Python en un \textit{shell}.
	
	\subsubsection{Objetos y tipos de datos}
	Los programas en Python manipulan objetos, es decir, datos, y estos tienen 
	un 
	tipo que define las operaciones que los programas pueden realizar sobre 
	ellos. Los tipos pueden ser escalares o no escalares. Para determinar el 
	tipo de un objeto se puede aplicar la función 
	incorporada \verb|type|.
	
	Un objeto escalar es indivisible, es decir atómico. Los tipos de objetos 
	escalares en Python son:
	\begin{itemize}
		\item \verb|int|. Representa números enteros, por ejemplo \verb|-5|.
		\item \verb|float|. Representa números reales; siempre incluye un punto 
		decimal, por ejemplo \verb|-5.34|.
		\item \verb|bool|. Usado para representar los valores booleanos 
		\verb|True| y \verb|False|.
		\item \verb|None|. Un tipo con un único valor. Símil a \verb|Null| en 
		Java.
	\end{itemize}
	
	Los objetos no escalares tienen una estructura interna, son 
	un conjunto de elementos. Estos son:
	\begin{itemize}
		\item \verb|list|. Una colección ordenada, por ejemplo 
		\verb|[1, 2 , 3]|.
		\item \verb|tuple|. Una colección ordenada inmutable, por ejemplo 
		\verb|(1, 2, 3)|.
		\item \verb|dict|. Un indexado (valor, clave) sin orden, por ejemplo 
		\verb|('a':1,	'b':2, 'c':3)|.
		\item \verb|set|. Una colección sin orden de valores únicos, por ejemplo
		\verb|(1, 2, 3)|.
		\item \verb|string|. Una concatenación de caracteres\footnote{No hay un 
			tipo \lstinline!char! en Python. Un \lstinline!string! de un solo 
			carácter es utilizado con el mismo sentido.}, o texto, por ejemplo 
		\verb|'abc'|.
	\end{itemize}	
	
	\subsubsection{Variables y asignación}
	En Python una variable no es más que un nombre. 
	Una sentencia de asignación asocia el nombre a la izquierda del símbolo 
	\textquote{=} 
	con el objeto a la derecha del mencionado símbolo, por ejemplo 
	\verb|pi = 3.14|. 
%	Además, Python permite asignación múltiple. La 
%	expresión \verb|x, y = 2, 3| enlaza \lstinline!x! con 2 y 
%	\lstinline!y! con 3.
	
	Los nombres de variables pueden contener letras en mayúsculas y minúsculas, 
	dígitos (sin comenzar con uno), y el carácter especial guión 
	bajo \textquote{\_}. Los nombres son \textit{case-sensitive}. 
	
	Existe una pequeña cantidad de palabras reservadas. En Python 3 son 
	\texttt{\justify and,
		as, assert, break, class, continue, def, del, elif, else, except, 
		False, 
		finally, for, from, global, if, import, in, is, lambda, nonlocal, None, 
		not, or, pass, raise, return, True, try, while, with} y \verb|yield|.	
	
	
	\section{Entorno de desarrollo}
	Un entorno de desarrollo, o ambiente de trabajo, contiene todo lo 
	requerido, por un individuo o 
	equipo, para desarrollar y desplegar sistemas de software. Tener una 
	definición coherente y completa de un entorno de desarrollo asegura que 
	nada se pasa por alto durante todo el ciclo de vida del software y libera 
	la carga laboral e intelectual de las personas. De acuerdo 
	a~\textcite{EelesDefinescopeyour2011}, son seis los 
	elementos que comprenden un entorno de desarrollo: método, herramientas, 
	capacitación, organización, infraestructura y 
	adopción.
	
	Desde el enfoque del código fuente Python, en su más básica representación 
	un entorno de desarrollo consiste en la combinación de un editor de texto y 
	un intérprete. Además, una serie de herramientas pueden ser añadidas al 
	entorno para automatizar prácticamente cualquier tarea, como ser pruebas 
	unitarias, conformidad con estándares y mejores prácticas, empaquetado, 
	control de versionamiento, entre otros.
	
	A continuación se presenta una selección de las 
	herramientas del entorno de desarrollo utilizadas en el presente trabajo. 
	Primeramente se expone Git, el sistema de control de versionamiento por 
	antonomasia, para luego continuar con pyenv, un administrador de versiones 
	de Python, y finalmente pipenv, el gestionador de paquetes recomendado por 
	la \acrshort{psf}.
	
	\subsection{Git}
	Se llama control de versiones a la gestión de los cambios a un archivo o 
	conjunto de archivos a lo largo del tiempo, de modo que se pueda recuperar 
	versiones específicas más adelante.
	
	Usualmente el método adoptado por la gente para llevar un control de las 
	versiones de sus archivos es copiar los mismos a otro directorio, o 
	renombrarlos añadiendo algún identificador. A pesar de su simpleza, este 
	método es altamente propenso a errores. Por esta razón hace ya muchos años 
	se desarrollaron \acrfull{vcs}, los cuales permiten: revertir tanto una 
	selección de archivos como el 
	proyecto entero a un estado anterior, comparar cambios a lo largo del 
	tiempo, ver quién modificó por última vez algo que puede estar causando un 
	problema, quién introdujo un error y cuándo, y mucho más.
	
	Existen tres clases de \acrshort{vcs}'s: locales, centralizados y 
	distribuidos. Los locales almacenan cambios en un solo lugar, mientras que 
	los centralizados siguen una topología cliente-servidor. Por último. los 
	distribuidos son una especie de híbrido entre los dos anteriores.
	
	Git es el \acrshort{vcs} distribuido por antonomasia, mediante el cual los 
	clientes no solo descargan\footnote{Coloquialmente se dice: 
		hacer un \textit{checkout}.} la última instantánea de los archivos, 
		sino 
	que 
	replican completamente el repositorio, asegurando que en el hipotético 
	caso de la caída de un servidor, cualquier repositorio de los clientes 
	puede ser la fuente para restaurar el servidor.  Concebido en el año 2005 
	desde el 
	seno de la comunidad Linux a la cabeza de Linus Torvalds, Git ha 
	evolucionado y madurado hasta convertirse actualmente en el VCS más popular 
	\parencite{GoogleGoogleTrends2018}.
	
	La fuente de los conceptos presentados en esta sección proviene de 
	\textcite{ChaconProGiteverything2014}, y allí se puede encontrar 
	información exhaustiva de todas las bondades de Git.
	
	\subsection{pyenv}
	Doug McIlroy, uno de los fundadores de la tradición UNIX, en \textit{A 
		quarter century of UNIX}~\parencite{SalusquartercenturyUNIX1994} resume 
	la 
	filosofía de UNIX de la siguiente manera:
	\begin{itemize}
		\item Escribe programas que hagan una cosa y la hagan bien.
		\item Escribe programas que trabajen en armonía.
		\item Escribe programas que manejen flujos de texto, pues ésta es una 
		interfaz universal.
	\end{itemize}
	
	Pyenv, es precisamente ese programa 
	que sigue la primera premisa de la 
	filosofía. Permite ejecutar múltiples versiones de Python, aisladas de la 
	versión de Python del sistema, y facilita la instalación, 
	administración e intercambio entre esas múltiples versiones de Python. En 
	síntesis, pyenv es un administrador simple de versiones de Python. Una 
	sesión típica de pyenv en bash se muestra en la \autoref{cod:ejemplopyenv}.
	
	\begin{figure}[htb]
		\centering		
		\begin{lstlisting}[style=bash]
	# Listando versiones de Python
	$ pyenv versions
	* system (set by /home/amartin1911/.pyenv/version)
	# Verificando la versión activa
	$ python -- version
	Python 2.7.14
	# Instalando una versión distinta
	$ pyenv install 3.5.4	
	# Activando la nueva versión instalada
	$ pyenv global 3.5.4			
	$ pyenv versions
	system 
	* 3.5.4 (set by /home/amartin1911/.pyenv/version)	
	$ python --version
	Python 3.5.4		
		\end{lstlisting}
		\caption{Ejemplo uso de pyenv}
		\label{cod:ejemplopyenv}
	\end{figure}
	
	El beneficio adicional de utilizar pyenv es que que se integra 
	automáticamente con pipenv ---otra herramienta que se describe en el 
	siguiente párrafo---. El resultado de esta integración es que pipenv 
	instalará automáticamente versiones faltantes de Python si son requeridas 
	por el archivo Pipfile
%	\todo{describir muy sucíntamente el pipfile}.
	
	\subsection{pipenv}
	Pipenv es un envoltorio (\textit{wrapper}) para tres tecnologías 
	ampliamente conocidas en Python: pip, pipfile y virtualenv. 
	pipenv es un administrador de dependencias y entornos virtuales para 
	proyectos en Python que proporciona una directa y poderosa herramienta de 
	línea 
	de comandos similar a \lstinline!npm! de Node.js o \lstinline!bundler! de 
	Ruby. 
	
	Crea y administra automáticamente el entorno virtual, conocido como 
	\textit{virtualenv}, para un 
	proyecto Python, además de agregar/eliminar paquetes del 
	\lstinline|Pipfile|\footnote{\lstinline|Pipfile| y su hermano 
	\lstinline|Pipfile.lock| son un reemplazo para el archivo estándar 
	\lstinline|requirements.txt| del gestor de paquetes \lstinline|pip|.} 
	mientras 
	el desarrollador instala/desinstala paquetes. También genera el archivo 
	\lstinline|Pipfile.lock| el cual es vital para producir compilados, es 
	decir, \textit{builds} 
	determinísticos. En la \autoref{cod:ejemplopipenv} se observa algunos de 
	los comandos esenciales para el uso de pipenv.
	
	\begin{figure}[htb]
		\centering		
		\begin{lstlisting}[style=bash]
	# Instalando pipenv
	$ pip install pipenv
	# Instalando paquetes. Por ejemplo, SQLAlchemy
	$ pipenv install sqalchemy
	# Gráfico de dependencias de paquetes
	$ pipenv graph
	graphviz==0.8.2     
	pylint==1.8.3       
	-astroid [required: >=1.6,<2.0, installed: 1.6.2]
	-lazy-object-proxy [required: Any, installed: 1.3.1]		
	pyserial==3.4       
	SQLAlchemy==1.2.5
	# Activando el entorno virtual
	$ pipenv shell	
		\end{lstlisting}
		\caption{Ejemplo uso de pipenv}
		\label{cod:ejemplopipenv}
	\end{figure}		
		
	\section{Kit GTK+}
	Hoy en día las interfaces de usuario se desarrollan con elementos 
	reutilizables de control gráfico, denominados widgets, que habilitan la 
	creación de ventanas, menús desplegables y una gran variedad de mecanismos 
	de interacción entre el usuario y la computadora. 
	
	\acrfull{gtk} es un kit de widgets multiplataforma 
	para crear interfaces gráficas de usuario. Cada 
	interfaz de usuario creada con GTK+ consiste en una organización 
	jerárquica de widgets implementados en C con GObject, un framework 
	orientado a objetos para C. El widget window (ventana) es el principal 
	contenedor. Luego la interfaz de usuario se construye añadiendo botones, 
	menús desplegables, campos de entrada y otros widgets a la ventana. Si la 
	interfaz de usuario es compleja, en vez de ensamblar la interfaz 
	manualmente es recomendable utilizar GtkBuilder y su lenguaje de marcado 
	específico para GTK+. También es posible utilizar Glade, un editor visual 
	de 
	interfaces de usuario \parencite{GNOMEGettingStartedGTK2018}.
	
	GTK+ es guiado por eventos. El kit \textquote{escucha} eventos, 
	como el clic de un botón o la entrada de texto, y los pasa al hilo 
	principal de la aplicación para su administración.
	
	\subsection{Historia y licencia}
	\acrshort{gtk} fue originalmente diseñado para el editor de imágenes raster 
	GIMP por Peter Mattis, Spencer Kimball y Josh MacDonald en 1997 mientras 
	trabajaban en la Universidad de California, Berkeley. 
	
	\acrshort{gtk} es más conocido por ser utilizado en GNOME, el entorno de 
	escritorio \textit{de-facto} en las mayores 
	distribuciones Linux, como ser, Fedora, Debian y Ubuntu. A pesar que 
	originalmente era usado solo en Linux, ha sido extendido y soporta otros 
	sistemas operativos como Microsoft Windows, BeOS, Solaris, Mac OS X, 
	entre otros. 
	
	Está licenciado bajo la LGPL, diferenciándose así de otros kit de 
	herramientas gráficas open source debido a que la licencia LGPL es más 
	fácil de usar junto con software propietario. Esto permite que el entorno 
	de escritorio GNOME sea una opción popular en la industria comercial. Otro 
	kit de widgets ampliamente utilizado en el mundo open source es Qt, pero su 
	licencia para uso comercial es restrictiva. Destacadas aplicaciones como 
	Firefox e Inkscape usan GTK+.
	
	\subsection{Arquitectura} 
	\acrshort{gtk} depende de múltiples librerías, las cuales se pueden 
	observar en la \autoref{fig:arquitecturagtk}. 
	Cada una de estas proporciona al desarrollador de una clase específica de 
	funcionalidad, tanto a bajo como alto nivel. 
	
	\begin{figure}[htb]
		\centering
		\def\svgwidth{0.7\columnwidth}
		\input{figuras/arquitecturagtk.pdf_tex}
		\caption{Arquitectura simplificada GTK+}
		\small{Fuente: Basado en~\textcite{GTK+GTKOverview2018}}
		\label{fig:arquitecturagtk}	
	\end{figure}
	
	A continuación se presenta una descripción de las principales líbrerias del 
	entorno GTK+, descritas por \textcite{KrauseFoundationsGTKdevelopment2007}.
	\begin{itemize}
		\item GLib. Una librería de utilidades de propósito general usada para 
		la implementación de características no-gráficas, como ser tipos de 
		datos, gestión de archivos, \textit{pipes}, hilos, y muchas más. GLib 
		también puede ser utilizada independientemente fuera del contexto de 
		GTK+.
		
		Uno de los mayores beneficios de GLib es que provee una interfaz 
		multiplataforma que permite ejecutar código, en cualquiera de los 
		sistemas soportados, prácticamente sin refactorización.
		
		\item GObject. GLib Object System implementa el sistema orientado a 
		objetos \lstinline!GType!, es cual es fundamental para la estructura 
		jerárquica de widgets de GTK+ como también para muchos de los objetos 
		implementados con sus librerías de apoyo. También proporciona los 
		sistemas de propiedades y 
		señales para los widgets, y gestión de la memoria.
		
		Originalmente GObject era parte de la librería de GTK+ 1. Con el 
		lanzamiento de GTK+ 2 fue separada a su propia librería y distribuida 
		junto con GLib.
		
		\item GIO. Gnome Input/Output es un sistema de archivos virtual (VFS) 
		con APIs para acceder y manipular archivos de una manera uniforme y 
		consistente a través de distintos sistemas operativos, como Windows, 
		MacOS X, Android e iOS.
		
		\item GDK. GIMP Drawing Kit (GDK) es un wrapper para las funciones de 
		bajo nivel de dibujo y ventanas provistas por el sistema operativo. En 
		Linux, GDK es un intermediario entre Xlib\footnote{Librería del Sistema 
			de Ventanas X.} y GTK+.
		
		GDK solía dibujar widgets, gráficos ráster, cursores y fuentes en todas 
		las aplicaciones GTK+. Desde GTK+ 3, la API de dibujo con GDK ha sido 
		removida y el dibujado, es decir, el renderizado, se realiza solo con 
		Cairo \parencite{GTK+MigratingGTKGTK}.
		
		\item GdkPixbuf. Una pequeña librería que proporciona funciones de 
		manipulación de imágenes del lado del cliente. Es utilizada para crear 
		widgets de tipo \lstinline!GtkImage!.
		
		\item Cairo. Incluida en GTK+ desde la versión 2.8, es una 
		librería para la creación de gráficos vectoriales en 2D. Además, como 
		se mencionó anteriormente, desde GTK+ 3 Cairo es la librería encargada 
		de renderizar widgets.
		
		\item Pango. Librería para el diseño y renderizado de texto con énfasis 
		en la internacionalización. Todo el texto renderizado con Pango esta 
		representado internamente con la codificación UTF-8.
		
		\item ATK. El kit de accesibilidad. Provee herramientas e interfaces 
		para las personas con capacidades diferentes.
		
	\end{itemize}
	
	Para más información acerca de todas las librerías y tecnologías del 
	proyecto GTK+, y el entorno GNOME, se recomienda visitar las páginas del 
	Centro del Desarrollador de GNOME (\url{https://developer.gnome.org/}), 
	Tecnologías GNOME (\url{https://www.gnome.org/technologies/}) y 
	documentación oficial de GTK+ (\url{https://www.gtk.org/documentation.php}).
	
	\subsection{PyGObject: Interfaces GTK+ con Python}
	El diseño de \gls{gtk} ha permitido que la librería, gracias a GObject, 
	haya 
	sido adaptada a diversos lenguajes de programación. En general esta 
	adaptación, o enlace de una librería hacia otros lenguajes se conoce como 
	\textit{language binding}. Los lenguajes con soporte oficial por parte de 
	GNOME, es decir, que cuentan 
	con una API estable y lanzamientos 
	planificados, son C++, Javascript, Python y Vala. Una lista completa se 
	puede revisar en \url{https://www.gtk.org/language-bindings.php}.
	
	PyGObject es el paquete de Python oficial para desarrollar aplicaciones 
	GTK+ 3. Soporta Linux, Windows y macOS, además trabaja con Python 2.7+ y 
	Python 3.4+. PyGObject, así como toda su documentación, están sujetas a la 
	licencia LGPL v2.1+ \parencite{PyGObjectOverviewPyGObject2017}.
	
	En la \autoref{fig:pygobjectoverview} se observa la interacción de GTK+ con 
	PyGObject, el cual utiliza \lstinline|glib, gobject, girepository, libfi| y 
	otras librerías más para acceder a la librería en C \lstinline!libgtk-3.so! 
	en 
	combinación con los metadatos adicionales del archivo tipo typelib 
	\verb|Gtk-3.0.typelib|, proporcionando así una API para Python.
	
	\begin{figure}[htb]
		\centering
		\includegraphics[height=5cm]{pygobjectoverview}
		\caption{Librerías de apoyo a PyGObject}
		\small{Fuente:~\textcite{PyGObjectOverviewPyGObject2017}}
		\label{fig:pygobjectoverview}	
	\end{figure}

	Documentación de la API de PyGObject se puede consultar en 
	\url{https://lazka.github.io/pgi-docs/}, y un tutorial completo con 
	ejemplos prácticos para la implementación de \textit{widgets} en 
	\url{https://python-gtk-3-tutorial.readthedocs.io/en/latest/}.
	
%	\section{Aplicaciones de rastreo de satélites}
%	\subsection{SatNOGS}
%	El proyecto SatNOGS (Satellite Network Open Ground Station) es una 
%	red de
%	estaciones terrenas Open Source construidas a partir de recursos y
%	herramientas fácilmente accesibles. Provee una plataforma modular y
%	escalable para comunicarse con satélites, principalmente con 
%	aquellos que
%	se encuentran en órbita LEO.
%	
%	Para la implementación de la plaforma, cuatro diferentes 
%	sub-proyectos con
%	funciones específicas son desarrollados.
%	\paragraph{Network (Red)}
%	SatNOGS Network es una aplicación web para programar observaciones 
%	a través
%	de la red de estaciones terrenas. Facilita (...)
%	\paragraph{Database (Base de Datos)}
%	\paragraph{Client (Cliente)}
%	
%	\subsection{GPredict}	
	
\end{document}